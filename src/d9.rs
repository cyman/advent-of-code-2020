use std::fs::File;
use std::io::{BufRead, BufReader};

const PREAMBLE_LENGTH: usize = 25;

fn main() {
    let file = File::open("src/input/day9.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<usize> = reader
        .lines()
        .into_iter()
        .map(|value| value.unwrap().parse::<usize>().unwrap())
        .collect();

    let invalid_number: usize = part_1(&lines);
    println!("Part 1: {}", invalid_number);
    println!("Part 2: {}", part_2(&lines, invalid_number));
}

fn part_1(lines: &Vec<usize>) -> usize {
    let mut preamble: Vec<usize> = Vec::new();

    for i in 0..PREAMBLE_LENGTH {
        preamble.push(lines[i]);
    }

    for i in PREAMBLE_LENGTH..lines.len() {
        if !is_sum_of_any_two(&preamble, lines[i]) {
            return lines[i];
        }

        preamble.remove(0);
        preamble.push(lines[i]);
    }

    0
}

fn part_2(lines: &Vec<usize>, invalid_number: usize) -> usize {
    for x in 0..lines.len() {
        for y in x + 1..lines.len() {
            let slice: &[usize] = &lines[x..y];
            let sum: usize = slice.to_vec().iter().sum();

            if sum == invalid_number {
                let min: usize = *slice.to_vec().iter().min().unwrap();
                let max: usize = *slice.to_vec().iter().max().unwrap();

                return min + max;
            }
        }
    }

    0
}

fn is_sum_of_any_two(preamble: &Vec<usize>, number: usize) -> bool {
    for (id_x, x) in preamble.iter().enumerate() {
        for (id_y, y) in preamble.iter().enumerate() {
            if id_x != id_y && x + y == number {
                return true;
            }
        }
    }

    false
}
