use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};

type LayerX = HashMap<isize, char>;
type LayerY = HashMap<isize, LayerX>;
type LayerZ = HashMap<isize, LayerY>;
type LayerW = HashMap<isize, LayerZ>;

fn main() {
    let file = File::open("src/input/day17.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader.lines().into_iter().map(|x| x.unwrap()).collect();

    println!("Part 1: {}", part_1(&lines));
    println!("Part 2: {}", part_2(&lines));
}

fn part_1(lines: &Vec<String>) -> usize {
    let initial_layer: Vec<Vec<char>> = lines.iter().map(|x| x.chars().collect()).collect();
    let mut pocket_dimension: LayerZ = HashMap::new();
    //initialise the pocket_dimension
    let mut layer_y: LayerY = HashMap::new();
    for (y, layer_line) in initial_layer.iter().enumerate() {
        let mut layer_x: LayerX = HashMap::new();
        for (x, cube) in layer_line.iter().enumerate() {
            layer_x.insert(x as isize, *cube);
        }
        layer_y.insert(y as isize, layer_x);
    }
    pocket_dimension.insert(0, layer_y);

    //perform the pocket dimension iterations
    for _ in 0..6 {
        let mut new_pocket_dimension: LayerZ = pocket_dimension.clone();

        for (x_id, y_id, z_id) in get_active_cubes_and_neighbours_locations(&pocket_dimension) {
            let active_neighbours = count_active_neighours(&pocket_dimension, x_id, y_id, z_id);
            let cube_value = get_cube_value(&pocket_dimension, x_id, y_id, z_id);

            if cube_value == '#' {
                if active_neighbours == 2 || active_neighbours == 3 {
                    set_cube_value(&mut new_pocket_dimension, x_id, y_id, z_id, '#');
                } else {
                    set_cube_value(&mut new_pocket_dimension, x_id, y_id, z_id, '.');
                }
            } else {
                if active_neighbours == 3 {
                    set_cube_value(&mut new_pocket_dimension, x_id, y_id, z_id, '#');
                } else {
                    set_cube_value(&mut new_pocket_dimension, x_id, y_id, z_id, '.');
                }
            }
        }

        pocket_dimension = new_pocket_dimension;
    }

    //count active cubes for the result
    let mut active_cube_count: usize = 0;

    for (_, y_layer) in pocket_dimension.iter() {
        for (_, x_layer) in y_layer.iter() {
            for (_, cube) in x_layer.iter() {
                if *cube == '#' {
                    active_cube_count += 1
                }
            }
        }
    }

    active_cube_count
}

fn part_2(lines: &Vec<String>) -> usize {
    let initial_layer: Vec<Vec<char>> = lines.iter().map(|x| x.chars().collect()).collect();
    let mut pocket_dimension: LayerW = HashMap::new();
    //initialise the pocket_dimension
    let mut layer_z: LayerZ = HashMap::new();
    let mut layer_y: LayerY = HashMap::new();
    for (y, layer_line) in initial_layer.iter().enumerate() {
        let mut layer_x: LayerX = HashMap::new();
        for (x, cube) in layer_line.iter().enumerate() {
            layer_x.insert(x as isize, *cube);
        }
        layer_y.insert(y as isize, layer_x);
    }
    layer_z.insert(0, layer_y);
    pocket_dimension.insert(0, layer_z);

    //perform the pocket dimension iterations
    for _ in 0..6 {
        let mut new_pocket_dimension: LayerW = pocket_dimension.clone();

        for (x_id, y_id, z_id, w_id) in
            get_active_cubes_and_neighbours_locations_4d(&pocket_dimension)
        {
            let active_neighbours =
                count_active_neighours_4d(&pocket_dimension, x_id, y_id, z_id, w_id);
            let cube_value = get_cube_value_4d(&pocket_dimension, x_id, y_id, z_id, w_id);

            if cube_value == '#' {
                if active_neighbours == 2 || active_neighbours == 3 {
                    set_cube_value_4d(&mut new_pocket_dimension, x_id, y_id, z_id, w_id, '#');
                } else {
                    set_cube_value_4d(&mut new_pocket_dimension, x_id, y_id, z_id, w_id, '.');
                }
            } else {
                if active_neighbours == 3 {
                    set_cube_value_4d(&mut new_pocket_dimension, x_id, y_id, z_id, w_id, '#');
                } else {
                    set_cube_value_4d(&mut new_pocket_dimension, x_id, y_id, z_id, w_id, '.');
                }
            }
        }

        pocket_dimension = new_pocket_dimension;
    }

    //count active cubes for the result
    let mut active_cube_count: usize = 0;

    for (_, z_layer) in pocket_dimension.iter() {
        for (_, y_layer) in z_layer.iter() {
            for (_, x_layer) in y_layer.iter() {
                for (_, cube) in x_layer.iter() {
                    if *cube == '#' {
                        active_cube_count += 1
                    }
                }
            }
        }
    }

    active_cube_count
}

fn count_active_neighours(pocket_dimension: &LayerZ, x: isize, y: isize, z: isize) -> usize {
    let mut count: usize = 0;

    for neighbour_z in vec![-1, 0, 1] {
        for neighbour_y in vec![-1, 0, 1] {
            for neighbour_x in vec![-1, 0, 1] {
                if neighbour_z == 0 && neighbour_y == 0 && neighbour_x == 0 {
                    continue;
                }

                if get_cube_value(
                    &pocket_dimension,
                    x + neighbour_x,
                    y + neighbour_y,
                    z + neighbour_z,
                ) == '#'
                {
                    count += 1;
                }
            }
        }
    }

    count
}

fn count_active_neighours_4d(
    pocket_dimension: &LayerW,
    x: isize,
    y: isize,
    z: isize,
    w: isize,
) -> usize {
    let mut count: usize = 0;

    for neighbour_w in vec![-1, 0, 1] {
        for neighbour_z in vec![-1, 0, 1] {
            for neighbour_y in vec![-1, 0, 1] {
                for neighbour_x in vec![-1, 0, 1] {
                    if neighbour_z == 0 && neighbour_y == 0 && neighbour_x == 0 && neighbour_w == 0
                    {
                        continue;
                    }

                    if get_cube_value_4d(
                        &pocket_dimension,
                        x + neighbour_x,
                        y + neighbour_y,
                        z + neighbour_z,
                        w + neighbour_w,
                    ) == '#'
                    {
                        count += 1;
                    }
                }
            }
        }
    }

    count
}

fn get_cube_value(pocket_dimension: &LayerZ, x: isize, y: isize, z: isize) -> char {
    let layer_z = pocket_dimension.get(&z);
    match layer_z {
        Some(map_z) => {
            let layer_y = map_z.get(&y);
            match layer_y {
                Some(map_y) => {
                    let cube = map_y.get(&x);
                    match cube {
                        Some(v) => return *v,
                        None => return '.',
                    }
                }
                None => return '.',
            }
        }
        None => return '.',
    }
}

fn get_cube_value_4d(pocket_dimension: &LayerW, x: isize, y: isize, z: isize, w: isize) -> char {
    let layer_w = pocket_dimension.get(&w);
    match layer_w {
        Some(map_w) => {
            return get_cube_value(&map_w, x, y, z)
        }
        None => return '.',
    }
}

fn get_active_cubes_and_neighbours_locations(
    pocket_dimension: &LayerZ,
) -> HashSet<(isize, isize, isize)> {
    let mut active_cubes: Vec<(isize, isize, isize)> = Vec::new();

    for (z_id, y_layer) in pocket_dimension.iter() {
        for (y_id, x_layer) in y_layer.iter() {
            for (x_id, cube) in x_layer.iter() {
                if *cube == '#' {
                    active_cubes.push((*x_id, *y_id, *z_id));
                }
            }
        }
    }

    let mut active_cubes_and_neighbours: HashSet<(isize, isize, isize)> = HashSet::new();

    for (x_id, y_id, z_id) in active_cubes {
        for neighbour_z in vec![-1, 0, 1] {
            for neighbour_y in vec![-1, 0, 1] {
                for neighbour_x in vec![-1, 0, 1] {
                    active_cubes_and_neighbours.insert((
                        x_id + neighbour_x,
                        y_id + neighbour_y,
                        z_id + neighbour_z,
                    ));
                }
            }
        }
    }

    active_cubes_and_neighbours
}

fn get_active_cubes_and_neighbours_locations_4d(
    pocket_dimension: &LayerW,
) -> HashSet<(isize, isize, isize, isize)> {
    let mut active_cubes: Vec<(isize, isize, isize, isize)> = Vec::new();

    for (w_id, z_layer) in pocket_dimension.iter() {
        for (z_id, y_layer) in z_layer.iter() {
            for (y_id, x_layer) in y_layer.iter() {
                for (x_id, cube) in x_layer.iter() {
                    if *cube == '#' {
                        active_cubes.push((*x_id, *y_id, *z_id, *w_id));
                    }
                }
            }
        }
    }

    let mut active_cubes_and_neighbours: HashSet<(isize, isize, isize, isize)> = HashSet::new();

    for (x_id, y_id, z_id, w_id) in active_cubes {
        for neighbour_w in vec![-1, 0, 1] {
            for neighbour_z in vec![-1, 0, 1] {
                for neighbour_y in vec![-1, 0, 1] {
                    for neighbour_x in vec![-1, 0, 1] {
                        active_cubes_and_neighbours.insert((
                            x_id + neighbour_x,
                            y_id + neighbour_y,
                            z_id + neighbour_z,
                            w_id + neighbour_w,
                        ));
                    }
                }
            }
        }
    }

    active_cubes_and_neighbours
}

fn set_cube_value(pocket_dimension: &mut LayerZ, x: isize, y: isize, z: isize, value: char) {
    let pocket_dimension_clone = pocket_dimension.clone();

    let layer_y = pocket_dimension_clone.get(&z);
    let new_y = HashMap::new();
    let map_y = layer_y.unwrap_or(&new_y);
    let mut map_y_clone = map_y.clone();

    let layer_x = map_y.get(&y);
    let new_x = HashMap::new();
    let map_x = layer_x.unwrap_or(&new_x);
    let mut map_x_clone = map_x.clone();

    map_x_clone.insert(x, value);
    map_y_clone.insert(y, map_x_clone);
    pocket_dimension.insert(z, map_y_clone);
}

fn set_cube_value_4d(
    pocket_dimension: &mut LayerW,
    x: isize,
    y: isize,
    z: isize,
    w: isize,
    value: char,
) {
    let pocket_dimension_clone = pocket_dimension.clone();

    let layer_z = pocket_dimension_clone.get(&w);
    let new_z = HashMap::new();
    let map_z = layer_z.unwrap_or(&new_z);
    let mut map_z_clone = map_z.clone();

    let layer_y = map_z.get(&z);
    let new_y = HashMap::new();
    let map_y = layer_y.unwrap_or(&new_y);
    let mut map_y_clone = map_y.clone();

    let layer_x = map_y.get(&y);
    let new_x = HashMap::new();
    let map_x = layer_x.unwrap_or(&new_x);
    let mut map_x_clone = map_x.clone();

    map_x_clone.insert(x, value);
    map_y_clone.insert(y, map_x_clone);
    map_z_clone.insert(z, map_y_clone);
    pocket_dimension.insert(w, map_z_clone);
}
