use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("src/input/day10.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<usize> = reader
        .lines()
        .into_iter()
        .map(|value| value.unwrap().parse::<usize>().unwrap())
        .collect();

    println!("Part 1: {}", part_1(&lines));
    println!("Part 2: {}", part_2(&lines));
}

fn part_1(lines: &Vec<usize>) -> usize {
    let mut joltage: usize = 0;
    let mut diff_1: usize = 0;
    let mut diff_3: usize = 0;
    let mut lines_clone: Vec<usize> = lines.clone();
    lines_clone.sort();

    for line in lines_clone.iter() {
        if line - 1 == joltage {
            diff_1 += 1;
            joltage += 1;
        } else {
            diff_3 += 1;
            joltage += 3;
        }
    }

    diff_1 * (diff_3 + 1)
}

fn part_2(lines: &Vec<usize>) -> usize {
    let mut lines_clone: Vec<usize> = lines.clone();
    lines_clone.sort();
    lines_clone.insert(0, 0);
    lines_clone.push(lines_clone.last().unwrap() + 3);

    let mut memo: HashMap<usize, usize> = HashMap::new();

    ways_to_get_to_end_from(&lines_clone, 0, &mut memo)
}

fn ways_to_get_to_end_from(
    lines: &Vec<usize>,
    index: usize,
    memo: &mut HashMap<usize, usize>,
) -> usize {
    if index == lines.len() - 1 {
        return 1;
    }

    if memo.contains_key(&index) {
        return *memo.get(&index).unwrap();
    }

    let mut ways = 0;

    for n in index + 1..lines.len() {
        if lines[n] as isize - lines[index] as isize <= 3 {
            ways += ways_to_get_to_end_from(&lines, n, memo);
        } else {
            break;
        }
    }

    memo.insert(index, ways);

    ways
}
