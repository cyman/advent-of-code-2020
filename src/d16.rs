use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug, Clone)]
struct InputData {
    ranges: Vec<Range>,
    my_ticket: Vec<usize>,
    nearby_tickets: Vec<Vec<usize>>,
}

#[derive(Debug, Clone)]
struct Range {
    field_name: String,
    ranges: Vec<(usize, usize)>,
}

fn main() {
    let file = File::open("src/input/day16.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader.lines().into_iter().map(|x| x.unwrap()).collect();
    let input_data: InputData = parse_input(&lines);

    println!("Part 1: {}", part_1(&input_data));
    println!("Part 2: {}", part_2(&input_data));
}

fn part_1(input_data: &InputData) -> usize {
    let mut count = 0;

    for nearby_ticket in &input_data.nearby_tickets {
        for ticket_number in nearby_ticket {
            let mut is_valid = false;
            for field_ranges in &input_data.ranges {
                for (min, max) in &field_ranges.ranges {
                    if ticket_number >= min && ticket_number <= max {
                        is_valid = true;
                    }
                }
            }
            if !is_valid {
                count += ticket_number;
            }
        }
    }

    count
}

fn part_2(input_data: &InputData) -> usize {
    let mut valid_nearby_tickets: Vec<Vec<usize>> = Vec::new();

    for nearby_ticket in &input_data.nearby_tickets {
        let mut is_valid = true;
        for ticket_number in nearby_ticket {
            let mut is_valid_for_ranges = false;
            for field_ranges in &input_data.ranges {
                for (min, max) in &field_ranges.ranges {
                    if ticket_number >= min && ticket_number <= max {
                        is_valid_for_ranges = true;
                    }
                }
            }
            if !is_valid_for_ranges {
                is_valid = false;
            }
        }
        if is_valid {
            valid_nearby_tickets.push(nearby_ticket.clone());
        }
    }

    let mut valid_field_ids: Vec<(String, Vec<usize>)> = Vec::new();
    for range in &input_data.ranges {
        valid_field_ids.push(find_valid_field_ids(&range, &valid_nearby_tickets));
    }

    let mut result_vec: Vec<String> = vec!["".to_owned();input_data.my_ticket.len()];

    for n in 1..=input_data.my_ticket.len() {
        for (field_name, field_ids) in &valid_field_ids {
            if field_ids.len() == n {
                for field_id in field_ids {
                    if result_vec[*field_id] == "".to_owned() {
                        result_vec[*field_id] = field_name.to_string();
                        break;
                    }
                }
            }
        }
    }

    let mut product = 1;

    for (id, field_name) in result_vec.iter().enumerate() {
        if field_name.starts_with("departure") {
            product *= input_data.my_ticket[id];
        }
    }

    product
}

fn find_valid_field_ids(range: &Range, nearby_tickets: &Vec<Vec<usize>>) -> (String, Vec<usize>) {
    let mut valid_field_ids: Vec<usize> = Vec::new();

    for n in 0..nearby_tickets[0].len() {
        let mut is_valid = true;
        for nearby_ticket in nearby_tickets {
            let mut is_valid_for_ranges = false;
            for (min, max) in &range.ranges {
                if nearby_ticket[n] >= *min && nearby_ticket[n] <= *max {
                    is_valid_for_ranges = true;
                }
            }

            if !is_valid_for_ranges {
                is_valid = false;
            }
        }
        if is_valid {
            valid_field_ids.push(n);
        }
    }

    (range.field_name.clone(), valid_field_ids)
}

fn parse_input(lines: &Vec<String>) -> InputData {
    let mut line_pointer: usize = 0;

    //get ranges
    let mut ranges: Vec<Range> = Vec::new();
    loop {
        if lines[line_pointer] == "" {
            break;
        } else {
            ranges.push(parse_ranges(&lines[line_pointer]));
        }
        line_pointer += 1;
    }

    //get my ticket
    line_pointer += 2;
    let my_ticket = parse_ticket(&lines[line_pointer]);

    //get nearby tickets
    let mut nearby_tickets: Vec<Vec<usize>> = Vec::new();
    line_pointer += 3;
    while line_pointer != lines.len() {
        nearby_tickets.push(parse_ticket(&lines[line_pointer]));
        line_pointer += 1;
    }

    InputData {
        ranges,
        my_ticket,
        nearby_tickets,
    }
}

fn parse_ranges(line: &String) -> Range {
    let split: Vec<&str> = line.split(": ").collect();
    let field_name: String = split[0].to_owned();
    let ranges_str: String = split[1].to_owned();
    let ranges_split: Vec<&str> = ranges_str.split(" or ").collect();
    let mut ranges: Vec<(usize, usize)> = Vec::new();

    for range_split in ranges_split {
        let range_split_str = range_split.to_owned();
        let range_value_split: Vec<&str> = range_split_str.split('-').collect();
        ranges.push((
            range_value_split[0].to_owned().parse::<usize>().unwrap(),
            range_value_split[1].to_owned().parse::<usize>().unwrap(),
        ));
    }

    Range { field_name, ranges }
}

fn parse_ticket(line: &String) -> Vec<usize> {
    let line_split: Vec<&str> = line.split(',').collect();
    line_split
        .iter()
        .map(|x| x.to_owned().parse::<usize>().unwrap())
        .collect()
}
