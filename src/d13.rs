use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("src/input/day13.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader.lines().into_iter().map(|x| x.unwrap()).collect();

    println!("Part 1: {}", part_1(&lines));
    println!("Part 2: {}", part_2(&lines));
}

fn part_1(lines: &Vec<String>) -> usize {
    let earliest_departure: usize = lines[0].parse::<usize>().unwrap();
    let bus_numbers: Vec<usize> = lines[1]
        .split(',')
        .into_iter()
        .filter(|x| *x != "x")
        .map(|x| x.to_owned().parse::<usize>().unwrap())
        .collect();
    let mut earliest_bus_departure: usize = usize::MAX;
    let mut earliest_bus_id: usize = 0;

    for bus_number in bus_numbers.iter() {
        let mut bus_schedule = bus_number.clone();

        while bus_schedule < earliest_departure {
            bus_schedule += bus_number.clone();
        }

        if bus_schedule < earliest_bus_departure {
            earliest_bus_departure = bus_schedule;
            earliest_bus_id = bus_number.clone();
        }
    }

    (earliest_bus_departure - earliest_departure) * earliest_bus_id
}

fn part_2(lines: &Vec<String>) -> isize {
    let mut bus_numbers_and_offsets: Vec<(isize, isize)> = Vec::new();

    for (offset, bus_number) in lines[1].split(',').into_iter().enumerate() {
        if bus_number != "x" {
            bus_numbers_and_offsets.push((bus_number.parse::<isize>().unwrap(), offset as isize));
        }
    }

    let mut current_timestamp = 0;
    let mut current_increment = 1;

    //calculate the timestamp which fits the conditions for the first two buses, then change the
    //increment to the lowest common multiple of bus numbers which fit the constraints, because that's 
    //when the situation will repeat and you can check if it's correct for 3 buses now, 
    //repeat until all the buses fit the constraints
    for n in 2..=bus_numbers_and_offsets.len() {
        loop {
            let mut is_correct_for_n_numbers = true;

            for x in 0..n {
                if (current_timestamp + bus_numbers_and_offsets[x].1) % bus_numbers_and_offsets[x].0
                    != 0
                {
                    is_correct_for_n_numbers = false;
                    break;
                }
            }

            if is_correct_for_n_numbers {
                break;
            }

            current_timestamp += current_increment;
        }

        //since our bus numbers are all prime, the lowest common multiple is a product of bus
        //numbers
        let mut product = 1;

        for x in 0..n {
            product *= bus_numbers_and_offsets[x].0;
        }

        current_increment = product;
    }

    current_timestamp
}
