use regex::Regex;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("src/input/day18.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader.lines().into_iter().map(|x| x.unwrap()).collect();

    println!("Part 1: {}", part_1(&lines));
    println!("Part 2: {}", part_2(&lines));
}

fn part_1(lines: &Vec<String>) -> usize {
    let mut sum = 0;

    for line in lines {
        sum += evaluate_expression_p1(&line);
    }

    sum
}

fn part_2(lines: &Vec<String>) -> usize {
    let mut sum = 0;

    for line in lines {
        sum += evaluate_expression_p2(&line);
    }

    sum
}

fn evaluate_expression_p1(expression: &String) -> usize {
    let mut expr = expression.clone();

    loop {
        if !expr.contains("(") {
            break;
        }

        let re = Regex::new(r"\((?P<inside>([0-9]| |\+|\*)+)\)").unwrap();
        let caps = re.captures(expr.as_str()).unwrap();
        let inside = &caps["inside"].to_owned();
        let inside_bracket_result = evaluate_expression_p1(&inside);
        expr = expr.replacen(
            format!("({})", inside).as_str(),
            format!("{}", inside_bracket_result).as_str(),
            1,
        );
    }

    loop {
        let plus_location = expr.find("+").unwrap_or(usize::MAX);
        let multiply_location = expr.find("*").unwrap_or(usize::MAX);

        if plus_location == usize::MAX && multiply_location == usize::MAX {
            break;
        }

        if plus_location < multiply_location {
            let re = Regex::new(r"(?P<left>[0-9]+) \+ (?P<right>[0-9]+)").unwrap();
            let caps = re.captures(expr.as_str()).unwrap();
            let left: usize = caps["left"].to_owned().parse::<usize>().unwrap();
            let right: usize = caps["right"].to_owned().parse::<usize>().unwrap();
            expr = expr.replacen(
                format!("{} + {}", left, right).as_str(),
                format!("{}", left + right).as_str(),
                1,
            );
        } else {
            let re = Regex::new(r"(?P<left>[0-9]+) \* (?P<right>[0-9]+)").unwrap();
            let caps = re.captures(expr.as_str()).unwrap();
            let left: usize = caps["left"].to_owned().parse::<usize>().unwrap();
            let right: usize = caps["right"].to_owned().parse::<usize>().unwrap();
            expr = expr.replacen(
                format!("{} * {}", left, right).as_str(),
                format!("{}", left * right).as_str(),
                1,
            );
        }
    }

    expr.parse::<usize>().unwrap()
}

fn evaluate_expression_p2(expression: &String) -> usize {
    let mut expr = expression.clone();

    loop {
        if !expr.contains("(") {
            break;
        }

        let re = Regex::new(r"\((?P<inside>([0-9]| |\+|\*)+)\)").unwrap();
        let caps = re.captures(expr.as_str()).unwrap();
        let inside = &caps["inside"].to_owned();
        let inside_bracket_result = evaluate_expression_p2(&inside);
        expr = expr.replacen(
            format!("({})", inside).as_str(),
            format!("{}", inside_bracket_result).as_str(),
            1,
        );
    }

    loop {
        let plus_location = expr.find("+").unwrap_or(usize::MAX);

        if plus_location == usize::MAX {
            break;
        }

        let re = Regex::new(r"(?P<left>[0-9]+) \+ (?P<right>[0-9]+)").unwrap();
        let caps = re.captures(expr.as_str()).unwrap();
        let left: usize = caps["left"].to_owned().parse::<usize>().unwrap();
        let right: usize = caps["right"].to_owned().parse::<usize>().unwrap();
        expr = expr.replacen(
            format!("{} + {}", left, right).as_str(),
            format!("{}", left + right).as_str(),
            1,
        );
    }

    loop {
        let multiply_location = expr.find("*").unwrap_or(usize::MAX);

        if multiply_location == usize::MAX {
            break;
        }

        let re = Regex::new(r"(?P<left>[0-9]+) \* (?P<right>[0-9]+)").unwrap();
        let caps = re.captures(expr.as_str()).unwrap();
        let left: usize = caps["left"].to_owned().parse::<usize>().unwrap();
        let right: usize = caps["right"].to_owned().parse::<usize>().unwrap();
        expr = expr.replacen(
            format!("{} * {}", left, right).as_str(),
            format!("{}", left * right).as_str(),
            1,
        );
    }

    expr.parse::<usize>().unwrap()
}
