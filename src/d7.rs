use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug)]
struct Container {
    bag_type: String,
    amount: usize,
    items: Vec<Container>,
}

fn main() {
    let file = File::open("src/input/day7.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader
        .lines()
        .into_iter()
        .map(|value| value.unwrap())
        .collect();

    let mut containers: Vec<Container> = Vec::new();

    for line in lines.iter() {
        let contains_split: Vec<&str> = line.split(" contain ").collect();
        let mut bag_type: String = contains_split[0].to_string();
        let mut items_string: String = contains_split[1].to_string();
        bag_type.pop();
        items_string.pop();
        let mut items: Vec<Container> = Vec::new();

        if items_string != "no other bags" {
            let items_split: Vec<&str> = items_string.split(", ").collect();

            for item in items_split.iter() {
                let mut item_string: String = item.to_string();

                if item_string.chars().last().unwrap() == 's' {
                    item_string.pop();
                }

                let item_count: usize = item_string
                    .chars()
                    .nth(0)
                    .unwrap()
                    .to_string()
                    .parse::<usize>()
                    .unwrap();
                let item_name: String = item_string[2..].to_owned();

                let container_item = Container {
                    bag_type: item_name,
                    amount: item_count,
                    items: Vec::new(),
                };
                items.push(container_item);
            }
        }

        let container = Container {
            bag_type,
            amount: 1,
            items,
        };
        containers.push(container);
    }

    println!("Part 1: {}", part_1(&containers));
    println!("Part 2: {}", part_2(&containers));
}

fn part_1(containers: &Vec<Container>) -> usize {
    let mut counter: usize = 0;

    for container in containers.iter() {
        if look_for_container(&containers, &container.items, &"shiny gold bag".to_owned()) {
            counter += 1;
        }
    }

    counter
}

fn part_2(containers: &Vec<Container>) -> usize {
    let my_bag = get_container_by_bag_type(&containers, &"shiny gold bag".to_owned());
    calculate_bags_inside(&containers, &my_bag.items)
}

fn look_for_container(
    containers: &Vec<Container>,
    container_items: &Vec<Container>,
    seeked_container: &String,
) -> bool {
    for item in container_items.iter() {
        let container = get_container_by_bag_type(&containers, &item.bag_type);

        if container.bag_type == *seeked_container {
            return true;
        }

        if look_for_container(&containers, &container.items, &seeked_container) {
            return true;
        }
    }

    false
}

fn get_container_by_bag_type<'a>(
    containers: &'a Vec<Container>,
    bag_type: &String,
) -> &'a Container {
    for container in containers.iter() {
        if container.bag_type == *bag_type {
            return container;
        }
    }

    panic!("Container not found!");
}

fn calculate_bags_inside(
    containers: &Vec<Container>,
    container_items: &Vec<Container>,
) -> usize {
    let mut bag_count = 0;

    for container_item in container_items.iter() {
        bag_count += container_item.amount;
        let root_child_container = get_container_by_bag_type(&containers, &container_item.bag_type);
        bag_count += container_item.amount
            * calculate_bags_inside(&containers, &root_child_container.items);
    }

    bag_count
}
