use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("src/input/day11.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<Vec<char>> = reader
        .lines()
        .into_iter()
        .map(|value| value.unwrap().chars().collect())
        .collect();

    println!("Part 1: {}", part_1(&lines));
    println!("Part 2: {}", part_2(&lines));
}

fn part_1(lines: &Vec<Vec<char>>) -> usize {
    let mut map = lines.clone();

    loop {
        let mut new_map = map.clone();

        for (y_id, y_value) in map.iter().enumerate() {
            for (x_id, spot) in y_value.iter().enumerate() {
                if *spot == 'L' && count_occupied_adjacent_seats(&map, x_id, y_id) == 0 {
                    new_map[y_id][x_id] = '#';
                }

                if *spot == '#' && count_occupied_adjacent_seats(&map, x_id, y_id) >= 4 {
                    new_map[y_id][x_id] = 'L';
                }
            }
        }

        if map == new_map {
            break;
        } else {
            map = new_map;
        }
    }

    let mut occupied_seats = 0;

    for row in map.iter() {
        occupied_seats += row.iter().filter(|x| **x == '#').count();
    }

    occupied_seats
}

fn part_2(lines: &Vec<Vec<char>>) -> usize {
    let mut map = lines.clone();

    loop {
        let mut new_map = map.clone();

        for (y_id, y_value) in map.iter().enumerate() {
            for (x_id, spot) in y_value.iter().enumerate() {
                if *spot == 'L' && count_occupied_visible_seats(&map, x_id, y_id) == 0 {
                    new_map[y_id][x_id] = '#';
                }

                if *spot == '#' && count_occupied_visible_seats(&map, x_id, y_id) >= 5 {
                    new_map[y_id][x_id] = 'L';
                }
            }
        }

        if map == new_map {
            break;
        } else {
            map = new_map;
        }
    }

    let mut occupied_seats = 0;

    for row in map.iter() {
        occupied_seats += row.iter().filter(|x| **x == '#').count();
    }

    occupied_seats
}

fn count_occupied_adjacent_seats(map: &Vec<Vec<char>>, x: usize, y: usize) -> usize {
    let mut count = 0;
    let mut map_clone = map.clone();
    map_clone[y][x] = '.';

    let top_left_bound_x = std::cmp::max(x as isize - 1, 0) as usize;
    let top_left_bound_y = std::cmp::max(y as isize - 1, 0) as usize;
    let bottom_right_bound_x = std::cmp::min(x as isize + 1, map[0].len() as isize - 1) as usize;
    let bottom_right_bound_y = std::cmp::min(y as isize + 1, map.len() as isize - 1) as usize;

    for loc_x in top_left_bound_x..=bottom_right_bound_x {
        for loc_y in top_left_bound_y..=bottom_right_bound_y {
            if map_clone[loc_y][loc_x] == '#' {
                count += 1;
            }
        }
    }

    count
}

fn count_occupied_visible_seats(map: &Vec<Vec<char>>, x: usize, y: usize) -> usize {
    let mut count = 0;

    //left
    if x != 0 {
        for i in (0..=x - 1).rev() {
            if map[y][i] == '#' {
                count += 1;
                break;
            }
            if map[y][i] == 'L' {
                break;
            }
        }
    }
    //right
    for i in x + 1..=map[0].len() - 1 {
        if map[y][i] == '#' {
            count += 1;
            break;
        }
        if map[y][i] == 'L' {
            break;
        }
    }
    //top
    if y != 0 {
        for i in (0..=y - 1).rev() {
            if map[i as usize][x] == '#' {
                count += 1;
                break;
            }
            if map[i as usize][x] == 'L' {
                break;
            }
        }
    }
    //bottom
    for i in y + 1..=map.len() - 1 {
        if map[i][x] == '#' {
            count += 1;
            break;
        }
        if map[i][x] == 'L' {
            break;
        }
    }
    //bottom left
    let mut current_x = x as isize;
    let mut current_y = y as isize;
    loop {
        current_x -= 1;
        current_y += 1;
        if current_x < 0 || current_y == map.len() as isize {
            break;
        }
        if map[current_y as usize][current_x as usize] == '#' {
            count += 1;
            break;
        }
        if map[current_y as usize][current_x as usize] == 'L' {
            break;
        }
    }
    //bottom right
    let mut current_x = x as isize;
    let mut current_y = y as isize;
    loop {
        current_x += 1;
        current_y += 1;
        if current_x == map[0].len() as isize || current_y == map.len() as isize {
            break;
        }
        if map[current_y as usize][current_x as usize] == '#' {
            count += 1;
            break;
        }
        if map[current_y as usize][current_x as usize] == 'L' {
            break;
        }
    }
    //top left
    let mut current_x = x as isize;
    let mut current_y = y as isize;
    loop {
        current_x -= 1;
        current_y -= 1;
        if current_x < 0 || current_y < 0 {
            break;
        }
        if map[current_y as usize][current_x as usize] == '#' {
            count += 1;
            break;
        }
        if map[current_y as usize][current_x as usize] == 'L' {
            break;
        }
    }
    //top right
    let mut current_x = x as isize;
    let mut current_y = y as isize;
    loop {
        current_x += 1;
        current_y -= 1;
        if current_x == map[0].len() as isize || current_y < 0 {
            break;
        }
        if map[current_y as usize][current_x as usize] == '#' {
            count += 1;
            break;
        }
        if map[current_y as usize][current_x as usize] == 'L' {
            break;
        }
    }

    count
}
