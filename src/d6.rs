use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("src/input/day6.txt").unwrap();
    let reader = BufReader::new(file);

    let lines: Vec<String> = reader
        .lines()
        .into_iter()
        .map(|value| value.unwrap())
        .collect();

    println!("Part 1: {}", part_1(&lines));
    println!("Part 2: {}", part_2(&lines));
}

fn part_1(lines: &Vec<String>) -> u32 {
    let mut plane_yes_count: u32 = 0;
    let mut group_yes_answers: Vec<char> = Vec::new();

    for line in lines.iter() {
        if line != "" {
            for character in line.chars().into_iter() {
                if !group_yes_answers.contains(&character) {
                    group_yes_answers.push(character);
                }
            }
        } else {
            plane_yes_count += group_yes_answers.len() as u32;
            group_yes_answers.clear();
        }
    }

    plane_yes_count
}

fn part_2(lines: &Vec<String>) -> u32 {
    let mut plane_yes_count: u32 = 0;
    let mut group_answers: Vec<HashSet<char>> = Vec::new();

    for line in lines.iter() {
        if line != "" {
            let passenger_answers: HashSet<char> = line.chars().collect();
            group_answers.push(passenger_answers);
        } else {
            let first_person_answer = group_answers[0].clone();
            let mut intersection: HashSet<char> = first_person_answer;

            for passenger_answers in group_answers.iter() {
                intersection = intersection.intersection(&passenger_answers).copied().collect();
            }

            plane_yes_count += intersection.len() as u32;
            group_answers.clear();
        }
    }

    plane_yes_count
}
